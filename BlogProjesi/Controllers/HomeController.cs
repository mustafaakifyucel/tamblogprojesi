﻿using BlogProjesi.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogProjesi.Controllers
{
    public class HomeController : Controller
    {
        BlogProjesiDBEntities db = new BlogProjesiDBEntities();
        
        public ActionResult Anasayfa()
        {
            var model = db.Bloglar.OrderByDescending(x => x.BlogID);
            return View(model);
        }
        
        public ActionResult Hakkimizda()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Iletisim()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Icerik(int id)
        {
            //Veritabanında ki ilk ve son eklenen kayıtların tutulduğu değişkenler.
            var EnBuyukId = db.Bloglar.Max(o => o.BlogID);
            var EnKucukId = db.Bloglar.Min(o => o.BlogID);
            //Eğer sonraki bloğa geçmek isterse ve sonraki blog kayıtlarda yoksa 
            //son blogda kalmasını sağlayan kontrol yapısı.
            if (id > EnBuyukId)
            {
                id = id - 1;
                var model = db.Bloglar.Find(id);
                return View(model);
            }
            //Eğer önceki bloğa geçmek isterse ve önceki blog kayıtlarda yoksa yani
            //ilk eklenen blogda ise o blogda kalmasını sağlayan kontrol yapısı.
            else if (id < EnKucukId)
            {
                id = id + 1;
                var model = db.Bloglar.Find(id);
                return View(model);
            }
            //İçeriği id ile bulup kullanıcıya gösteren yapı.
            else
            {
                var model = db.Bloglar.Find(id);
                return View(model);
            }
        }

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
    }
}