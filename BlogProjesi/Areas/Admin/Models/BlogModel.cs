﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogProjesi.Areas.Admin.Models
{
    public class BlogModel
    {
        [Required]
        [Display(Name = "Blog Numarası")]
        public int BlogID { get; set; }
        [Required]
        [Display(Name = "Blog Başlığı")]
        public string BlogBaslik { get; set; }
        [Required]
        [Display(Name = "Blog Alt Başlığı")]
        public string BlogAltBaslik { get; set; }
        [Required]
        [Display(Name = "Blog İçeriği")]
        public string BlogIcerik { get; set; }
        [Required]
        [Display(Name = "Tarih")]
        public string Tarih { get; set; }
    }
}