﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogProjesi.Models;
using BlogProjesi.Areas.Admin.Models;

namespace BlogProjesi.Areas.Admin.Controllers
{
    public class AdminHomeController : Controller
    {
        BlogProjesiDBEntities db = new BlogProjesiDBEntities();
        // GET: Admin/AdminHome
        [Authorize(Users = "deneme@deneme.com")]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Users = "deneme@deneme.com")]
        public ActionResult Ekle()
        {
            return View();
        }

        [HttpPost]

        public ActionResult Ekle(BlogModel model)
        {
            /*if (ModelState.IsValid)
            {

            }*/

            Bloglar bloglar = new Bloglar();

            bloglar.BlogID = model.BlogID;
            bloglar.BlogBaslik = model.BlogBaslik;
            bloglar.BlogAltBaslik = model.BlogAltBaslik;
            bloglar.BlogIcerik = model.BlogIcerik;
            bloglar.Tarih = model.Tarih;

            db.Bloglar.Add(bloglar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [Authorize(Users = "deneme@deneme.com")]
        public ActionResult Listele()
        {
            var model = db.Bloglar.OrderByDescending(x => x.BlogID);
            return View(model);
        }

        [HttpGet]
        public ActionResult Duzenle(int id)
        {
            var model = db.Bloglar.Where(x => x.BlogID == id).FirstOrDefault();
            return View(model);
        }

        [HttpPost]
        public ActionResult Duzenle(Bloglar model)
        {
            /*if (ModelState.IsValid)
            {

            }*/

            var degistirilecek = db.Bloglar.Where(x => x.BlogID == model.BlogID).FirstOrDefault();

            degistirilecek.BlogID = model.BlogID;
            degistirilecek.BlogBaslik = model.BlogBaslik;
            degistirilecek.BlogAltBaslik = model.BlogAltBaslik;
            degistirilecek.BlogIcerik = model.BlogIcerik;
            degistirilecek.Tarih = model.Tarih;

            //db.Bloglar.Attach(degistirilecek);

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Sil(int id)
        {

            var model = db.Bloglar.Where(x => x.BlogID == id).FirstOrDefault();
            return View(model);
        }
        [HttpPost]
        public ActionResult Sil(Bloglar model)
        {
            var silinecek = db.Bloglar.Where(o => o.BlogID == model.BlogID).FirstOrDefault();
            db.Bloglar.Remove(silinecek);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}